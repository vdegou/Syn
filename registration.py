import json
import os
import signal
import subprocess
import sys

from leap.soledad.client._secrets.util import SecretsError
from leap.soledad.client import Soledad
import daemon
import syn_utils


# Developing using Soledad's offline mode by setting server_url to None
server_url = None

base_db_dir = os.path.expanduser('~/.syn/dbs/')


class DatabaseException(Exception):
    pass


class DatabaseAlreadyOpenException(DatabaseException):
    pass


class InvalidCredentialsException(DatabaseException):
    pass


def _spawn_http_daemon():
    """
    Spawns a subprocess which encapsulates the Syn's HTTP daemon functionality. All Twisted functionality
    is stored in this daemon.
    """

    daemon_path = daemon.__path__.pop() + "/http_daemon.py"

    subprocess.Popen(["twistd", "-y", daemon_path])


def open_db(uuid, passphrase):
    """
    Logs in a user with the specified uuid and passphrase.
    :param uuid: user unique ID
    :param passphrase: passphrase used to cipher secrets and authenticate user
    :return: Soledad object
    :rtype: leap.soledad.client.Soledad
    """
    if not db_is_open():

        # Database directory
        db_dir = base_db_dir + uuid + '/'

        secrets_path = db_dir + 'secrets.json'
        cert_file = db_dir + 'ca.crt'
        token = db_dir + 'token'
        shared_db_path = db_dir + 'db.sqlite'

        # Error opening file
        if not os.path.exists(secrets_path):
            raise EnvironmentError("Error opening file at {}".format(secrets_path))

        try:

            client = Soledad(
                uuid,
                passphrase,
                secrets_path=secrets_path,
                local_db_path=shared_db_path,
                server_url=server_url,
                cert_file=cert_file,
                auth_token=token
            )

            # If we can make it past this point, it means we were able to log in.
            # Now we store a temporary file to denote that this user is authenticated
            auth_file = os.path.expanduser('~/.syn/auth_file')

            # TODO: URGENTLY replace this with something cryptographically secure.
            # We want something retrievable while OFFLINE (think hash, seed, prng, rng?)
            # Something that can be randomly written and recovered and authenticated by both.
            # This is to avoid someone simply creating the auth file and writing "authenticated" in it and fooling
            # the offline auth mechanism.
            with open(auth_file, 'w+') as cfg:
                params = {
                    'uuid': uuid,
                    'passphrase': passphrase,
                    'secrets_path': secrets_path,
                    'local_db_path': shared_db_path,
                    'server_url': None,
                    'cert_file': cert_file,
                    'auth_token': token
                }

                # Jsonify the client parameters and write them to auth file
                params = json.dumps(params)
                cfg.write(params)

            # Once a DB has been opened correctly, instantiate the HTTP daemon
            _spawn_http_daemon()

            return client

        # secrets.json is not properly formatted
        except ValueError:
            raise ValueError("%s is not properly formatted." % secrets_path)

        except (SecretsError, EnvironmentError):
            raise InvalidCredentialsException("Unable to decrypt secrets.json")

    else:
        raise DatabaseException("Database already open")


def close_db():
    """
    Closes a database if there is one currently open. Also kills the Syn Daemon in case it has spawned.
    """
    auth_file = os.path.expanduser('~/.syn/auth_file')

    if db_is_open():

        # Remove auth_file
        if os.path.exists(auth_file):
            os.remove(auth_file)

        # Kill Syn daemon
        if os.path.exists('./twistd.pid'):

            with open('./twistd.pid') as pid_file:
                twistd_pid = int(pid_file.read())
                os.kill(twistd_pid, signal.SIGTERM)

    else:
        raise DatabaseException("There is no open database")


def new_db(uuid, passphrase):
    """
    Creates a new, empty database. Also creates the data structure that holds all relevant files.

    :param uuid: unique database ID
    :param passphrase: passphrase used to encrypt and decrypt the database
    :return: None
    """
    # Database directory
    db_dir = base_db_dir + uuid + '/'

    try:
        # Create necessary user directory
        syn_utils.create_path_if_not_exists(db_dir)

        # When Soledad is in offline mode, secrets.json will be created on-the-fly at this path
        secrets_path = db_dir + 'secrets.json'
        cert_file = db_dir + 'ca.crt'
        token = db_dir + 'token'
        shared_db_path = db_dir + 'db.sqlite'

        # TODO: replace this with a function for creating a secrets.json while offline. It would remove the need
        # of instantiating a Soledad object just for the purpose of creating a secrets.json file.
        # Instantiating a Soledad object is really only useful in the login method.
        client = Soledad(
            uuid,
            passphrase,
            secrets_path=secrets_path,
            local_db_path=shared_db_path,
            server_url=server_url,
            cert_file=cert_file,
            auth_token=token
        )

    except OSError:
        e_type, value, traceback = sys.exc_info()
        raise DatabaseException, ('A database with that UUID already exists', e_type, value), traceback


def delete_db(uuid, passphrase):
    """
    Deletes a database. There must be no open database in order to delete one.
    """
    # Database directory
    db_dir = base_db_dir + uuid + '/'

    try:
        # Open the database first to check that uuid and passphrase are correct
        open_db(uuid, passphrase)

        # Delete the database files
        syn_utils.remove_path(db_dir)

        # Close the database
        close_db()

    # Database already open
    except DatabaseException:
        raise

    # Wrong credentials
    except InvalidCredentialsException:
        raise

    # Could not open secrets.json
    # Can happen when secrets.json doesn't exist OR if the database has not been created.
    except EnvironmentError:
        raise


def db_is_open():
    """
    Returns whether or not a database is open
    :return: True if a database is open, False otherwise
    """
    auth_file = os.path.expanduser('~/.syn/auth_file')
    return os.path.exists(auth_file)
