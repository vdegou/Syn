Architecture
============
Syn is made up of two main components, which act as its building blocks:

.. figure:: _assets/syn_building_blocks.png
    :scale: 100%
    :align: center

The two main components of Syn are `Soledad <https://soledad.readthedocs.io/en/latest/index.html>`_, which is a
solution for synchronizing encrypted data, and `Twisted <https://twistedmatrix.com/trac/>`_, which is an event-driven
networking framework for Python.

Syn uses Soledad as its data management engine, meaning all operations that have to do with the creation, deletion,
updating or selection of data is done via the Soledad component. Some benefits of using Soledad for this task are:

* Data encryption is done locally and handled completely by this component
* Contains mechanisms to synchronize data over the network, allowing for devices to share state

What Syn accomplishes with Soledad is to create an extra layer of functionality that allows the management
of multiple different local database instances. A use case for this would be for different users on the same machine
to be able to manage their own separately encrypted password stores, or even for the same user to split their passwords
into different organizational levels (passwords used at home, for work, entertainment, etc).

The typical flow behind the execution of Syn looks something like this:

.. figure:: _assets/syn_simple_flow.png
    :scale: 100%
    :align: center

Basically, executing the core Syn process will spawn a daemon process in the background. The daemon process is,
essentially, a local HTTP server. The core process communicates with this daemon via an HTTP API, and the daemon
executes the desired action on the locally encrypted database and returns a response. The decision to include an
HTTP daemon will be explained with greater detail in the :ref:`HTTP Daemon <http-daemon>` section.

.. _http-daemon:

HTTP Daemon
-----------
The decision to include this HTTP daemon was to decouple the code as well as to make it more functional and well-aligned
with the Twisted framework. By doing so, Syn's architecture can be split up into two components and their concerns
separated. Now, the core process can cleanly deal with filesystem related things, while the HTTP daemon can deal with
all things Soledad and TWisted, and they both communicate through a minimal API. The following diagram illustrates
this relationship:

.. figure:: _assets/syn_detailed_flow.png
    :align: center

In the diagram, we have the following entities:

- **Core Syn Process:** this process takes care of all the filesystem related responsibilities, such as creating and
  deleting local databases which are, essentially, encrypted files. It is also in charge of "opening" or decrypting
  the databases and keeping track of the state of the database. This process is presented as a command line interface.

- **Syn HTTP Daemon:** technically, this is a fully fledged
  `Twisted Web Application <https://twistedmatrix.com/documents/current/core/howto/application.html#>`_. As specified,
  this component takes care of all the Twisted and Soledad related functionality with respect to accessing the locally
  encrypted database instances.

- **Local encrypted database:** this is the database file stored in the local filesystem which has been encrypted by
  the Soledad engine.

Taking a look at the diagram, a normal execution would be constructed as follows:

**Spawning the core process and HTTP daemon**

   #. The Core Syn Process is instantiated via the command line
   #. Syn is instructed to open a database and authentication details such as the database's UUID and passphrase are provided
   #. The database is decrypted and the ``auth_file`` is created, which keeps the state of the database locally
   #. The Syn HTTP Daemon is spawned, exposing a local HTTP API.

**Communicating with the HTTP daemon**

   #. The Core Syn Process, which is basically a CLI application, can send HTTP requests to the daemon via an exposed
      API. Each request maps to a certain functionality that wants to be executed, such as creation or deletion of passwords
   #. The API receives the request, routes the code flow to the corresponding response method and obtains a ``Deferred``
      object
   #. Once the ``Deferred`` object is obtained, the ``reactor`` resolves the ``Deferred`` by accessing the local database
      and encodes the results as JSON strings
   #. The result obtained from resolving the ``Deferred`` is sent back via the API as an HTTP response. The client, or
      Core Process can now do as it wishes with the result.

To learn more about ``Deferred`` objects and asynchronous work flows, check out
`this blog post <https://diffya.github.io/GSoC2018/2018/07/06/The-intricacies-of-yield.html>`_.

**Stopping Syn**

   #. The Core Syn Process is instructed to close the currently open database
   #. The database is closed by deleting the `auth_file`, which means there is no longer a record of the current state
      of an open database. If there is no state record, Syn assumes there does not exist an open database
   #. A kill signal is sent to the Syn HTTP Daemon, prompting it to clean up and terminate. Once this process has
      terminated, there is no way to interact via the local databases.