Using Syn
=========
Syn is meant to be used as a straightforward CLI application meant for quick and easy storage of important information
that is frequently used, such as passwords. In this section, a brief walkthrough is detailed on how to use the different
Syn functions. In order to follow this walkthrough, it is necessary to have installed Syn through `pip`. This setup
process is covered in the :ref:`Installation <installation>` section.

Creating a database
-------------------
To create a database, we execute the following command:

.. code-block:: bash

    $ syn new

This will result in a prompt requesting a UUID and passphrase for the database that is to be created. **Note:** a
database cannot have the same UUID as another one.

.. code-block:: bash

    $ syn new

    Creating a new database...
    UUID: my_db
    Passphrase:
    Repeat for confirmation:
    Successfully created a database!

Upon inserting a valid UUID and passphrase, the terminal should tell us that the database was created successfully.
Congratulations, you have created your first Syn database!


Opening the database
--------------------
The database that was just created is encrypted and stored locally. If we want to perform any operations in this database,
such as storing, reading, or deleting entries, we first have to "open" the database. Opening a database is equivalent
to authenticating yourself towards the Syn system, in order to prove that you know both the UUID and passphrase of said
database. Upon completing this step, the database operations will be able to be performed.

To open the database, simply execute the following command:

.. code-block:: bash

    $ syn open

A prompt will request the UUID and passphrase of the database you wish to open. Upon successfully entering the
requested information, the database will open and the following message will be prompted:

.. code-block:: bash

    $ syn open

    UUID: my_db
    Passphrase:
    Database has been opened

Inserting entries into the database
-----------------------------------
To store an entry into the database, the following command must be executed:

.. code-block:: bash

    $ syn add

A prompt will appear requesting for information such as the name of the entry and if you would like to randomly
generate a password or provide one yourself.

.. code-block:: bash

    $ syn add

    Name the entry: Email
    Would you like to randomly generate a passphrase? [y/N]: y
    Enter the desired length of the passphrase [8]: 12
    Enter a generation seed []:

The name of the entry is very important, as it must be unique among all entries within the same database.
The entry name is used to identify entries and locate them within the database instance, so make sure you are using
names that are not already in use.

Getting a list of entries from the database
---------------------------------------------
Syn allows to get a list of all existing entries within the currently open database with a single command.
In order to obtain the list of existing entries, simply execute the following command:

.. code-block:: bash

    $ syn list

    Found 4 entries:
    Email
    Locker
    Phone
    Server


Reading an entry from the database
----------------------------------
Reading an entry from the database will allow you to obtain its details such as the content stored within the entry,
the date it was created, and the last time it was modified. To do so, the following command must be executed:

.. code-block:: bash

    $ syn get

With this operation, you only need to provide the name of the entry you wish to obtain. After that, the details of
the entry should be prompted on the command line interface:

.. code-block:: bash

    $ syn get

    Name of the entry you wish to obtain: Email
    Found the following entry:
    ─────────────────────────
    Name: Email
    Time of creation: 2018-07-31 14:00:11 UTC
    Time of last modification: 2018-07-31 14:00:11 UTC
    Contents of the entry have been copied to the system clipboard!
    ─────────────────────────

After getting the details of an entry from the database, the password that was stored with it will have been copied
to your system clipboard. This helps to make password lookup a convenient and quick task when using Syn.

Deleting entries from the database
-----------------------------------
Deleting an entry will remove the data from the local database. The process to deleting an entry is very similar to the
process for reading an entry. To delete an entry, the following command must be executed:

.. code-block:: bash

    $ syn delete

Syn will then prompt for the name of the entry, and upon entering the name of an existing entry, Syn will proceed to
remove the data from the database:

.. code-block:: bash

    $ syn delete

    Name of the entry you wish to delete: Email
    Entry was deleted successfully.

Closing a database
------------------
Closing a database will kill the :ref:`HTTP daemon <http-daemon>`  that grants access to the database, effectively
closing off communication between any program and the encrypted local database, including the Syn Core Process.
A database should be closed when it is no longer in use.

To close the database, execute the following command:

.. code-block:: bash

    $ syn close

Any open database will be closed and the following message will be prompted:

.. code-block:: bash

    $ syn close
    Database has been closed

Deleting a database
-------------------
Entire databases can be deleted using Syn. To do so, execute the following command:

.. code-block:: bash

    $ syn delete_db

Syn will prompt for authentication before deletion, to make sure you really want to delete the database.
Once a database has been deleted, the information cannot be recovered.

.. code-block:: bash

    $ syn delete_db

    Deleting a database... Press CTRL+C to abort.
    UUID: my_db
    Passphrase:
    Repeat for confirmation:
    Database has been deleted.
