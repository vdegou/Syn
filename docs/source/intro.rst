Introduction
============

Syn is an easy to use password management application developed using the Python programming language
and an underlying `Soledad <https://soledad.readthedocs.io/en/latest/index.html>`_ engine for data synchronization
and security. Furthermore, Soledad is built using the
`Twisted event-driven framework <https://twistedmatrix.com/trac/>`_. Both of these components allow Syn to work with
data that is encrypted as soon as it enters the system, as well as synchronizing this data across devices and allowing
for asynchronous processing.

Syn allows to create locally encrypted databases in the user's filesystem. Within these databases, Syn can store,
retrieve, and delete information in a secure way. These databases are meant to be used as secure password repositories,
where the user can quickly find an entry and copy the password to the system clipboard. Unlike many other password
managers out there today, Syn places great emphasis on being completely functional without needing an active
internet connection.

The functionality of Syn can be accessed via a command line interface, which for now, is the main entry point of
the application. However, since Syn exposes an API to make use of its functionalities and features, a GUI is planned
for some point in the future.
a locally encrypted database that holds the stored information via an HTTP server daemon.

