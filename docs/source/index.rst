.. Syn documentation master file, created by
   sphinx-quickstart on Fri Jul 27 09:46:12 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Syn
==========

Syn is an easy to use password management application developed using the Python programming language
and an underlying `Soledad <https://soledad.readthedocs.io/en/latest/index.html>`_ engine for data synchronization
and security.

Syn started as a project proposal for the `LEAP <https://leap.se/>`_ non-profit organization in the
`Google Summer of Code 2018 <https://summerofcode.withgoogle.com/>`_ program. The project is currently in development,
but it is mature enough to be used in its current state. The goal of the project is to create a fully-functional and
secure password management application which can be used to synchronize data across devices.


Table of Contents
=================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   intro
   installation
   architecture
   usage




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
