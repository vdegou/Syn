.. _installation:

Installation
=============

Syn is distributed as a Python package. As such, ``pip`` can be used to install it.

To install Syn, execute the following command in a terminal console:

.. code-block:: bash

    $ pip install syn

To check if Syn was installed correctly, execute the following command:

.. code-block:: bash

    $ syn --help

The following output should appear in your terminal console:

.. code-block:: bash

    Usage: syn [OPTIONS] COMMAND [ARGS]...

    Syn is a password manager which offers an easy to use interface and
    cryptographically secure storage of passwords and notes.

    Options:
    --help  Show this message and exit.

    Commands:
        add           Store a new entry in the database
        close         Closes a database
        delete_db     Deletes a database
        delete_entry  Delete an entry from the database
        get           Shows the details of an entry
        list          Lists the names of stored entries
        new           Creates a new database
        open          Opens the database
        passphrase    Generates a passphrase

Source Code
-----------
Source code for Syn can be found `here <https://0xacab.org/vdegou/Syn>`_.