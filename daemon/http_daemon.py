import json
import sys

from twisted.application.service import Application
from twisted.web.resource import Resource
from twisted.web.server import Site
from twisted.internet import reactor
from twisted.web import server, resource

from data import DataManager
from data import Entry
from registration import close_db
from syn_utils import create_client_from_file


# Port to bind HTTP server to
port = 5001

# Instantiate the Soledad object
# This assumes a DB is open, which is fine because that method executes this program
soledad = create_client_from_file()

# Instantiate the Data Manager
data_manager = DataManager(soledad)


class ListEntries(Resource):
    """
    Resource representing the get_list() method.
    API endpoint: /list
    """

    def render_GET(self, request):
        """
        Method which responds to GET requests made to this endpoint
        :param request: the request made to this resource
        :return: a JSON-encoded string with the form

        { entries: <list_entries> }

        """
        # Set response headers for the application/json content-type
        request.responseHeaders.addRawHeader(b'content-type', b'application/json')

        def cb(data):
            """
            Callback function which obtains the list of retrieved Document objects and puts their id's inside of a list,
            which is returned. The Document id's are equivalent to the Entry names.
            :param data: list of documents
            """
            list_docs = data[1]
            list_entries = []

            # Get the doc id's in a list
            for doc in list_docs:
                list_entries.append(doc.doc_id)

            # Format the response
            response = {
                "entries": list_entries
            }

            # Return the response as a JSON-encoded string
            request.write(json.dumps(response))
            request.finish()

        d = data_manager.list_entries(cb=cb)

        return server.NOT_DONE_YET


class GetEntry(Resource):
    """
    Resource representing the get_entry() method.
    API endpoint: /entry/<entry_id>
    """
    isLeaf = True

    def url_is_valid(self, url):
        """
        Makes sure the "postpath" url only contains one URL segment depicting the <entry_id> parameter
        :param url: url after the /entry/ segment
        """

        if len(url) is not 1:
            return False
        else:
            return True

    def render_GET(self, request):
        """
        Method which responds to GET requests made to this endpoint
        :param request: the request made to this resource
        :return: a JSON-encoded string with the form

        { content: <entry.content>,
          last_modified: <entry.last_modified>,
          name: <entry.name>,
          timestamp_creation: <entry.timestamp_creation>,
          uuid: <entry.uuid>
        }

        """

        def cb(doc):
            """
            Callback function which obtains a Document object from the deferred, obtains the Entry content
            which is wrapped inside as a JSON, and writes returns the content of the Entry.
            """

            response = {}

            if doc is not None:
                response = doc.get_json()
                request.write(str(response))

            # No document found by that name
            else:
                request.setResponseCode(404)  # Resource not found

                response = {
                    'status': 404,
                    'message': "Could not find an entry by that name."
                }

                request.write(json.dumps(response))

            request.finish()

        # request.setResponseCode(http.BAD_REQUEST)
        url = request.postpath

        if self.url_is_valid(url):

            # Set response headers for the application/json content-type
            request.responseHeaders.addRawHeader(b'content-type', b'application/json')

            # Get the url parameter
            entry_id = url.pop()

            # Obtain the deferred which contains the entry details and add the callback
            d = data_manager.get_entry(entry_id, cb=cb)

        else:
            request.setResponseCode(400)  # Bad request, URL is not properly formatted

            response = {
                'status': 400,
                'message': 'An invalid URL was provided. Please see documentation.' # TODO: documentation.
            }

            request.write(json.dumps(response))
            request.finish()

        return server.NOT_DONE_YET


class AddEntry(Resource):
    """
    Resource representing the add_entry() method.
    API endpoint: /add
    """
    isLeaf = True

    def validate_body(self, request, body):

        print("BODY: {}".format(body))
        print("BODY TYPE: {}".format(type(body)))

        try:

            body = json.loads(body)

            expected_keys = ['name', 'content', 'last_modified', 'timestamp_creation', 'uuid']

            if len(body.keys()) != len(expected_keys):
                request.setResponseCode(400)  # Badly formed input json

                response = {
                    'status': 400,
                    'message': 'The amount of specified keys is incorrect. Please see the documentation.'
                }

                request.write(json.dumps(response))
                request.finish()

                return False

            for keys in expected_keys:

                if keys not in body.keys():
                    request.setResponseCode(400)  # Badly formed input json

                    response = {
                        'status': 400,
                        'message': 'Did not find all the required keys in the request body. Please see the documentation.'
                    }

                    request.write(json.dumps(response))
                    request.finish()

                    return False

            return True

        except ValueError:  # If json.loads cannot load the body due to malformed string, return false
            return False

    def render_POST(self, request):
        """

        :param request: receives a JSON-encoded string in the body with the format

        { 'name': <name>,
          'content': <content>,
          'last_modified': <last_modified>,
          'timestamp_creation': <timestamp_creation>,
          'uuid': <uuid>
        }

        :return:
        """

        def cb(data):
            request.setResponseCode(200)  # Request was successful

            response = {
                'status': 200,
                'message': 'Entry was created successfully.'
            }

            request.write(json.dumps(response))
            request.finish()

        def eb(failure):
            request.setResponseCode(409)  # Conflict in the current state of the resource, e.g. might already exist

            response = {
                'status': 409,
                'message': 'There was an error when processing the request.'
            }

            request.write(json.dumps(response))
            request.finish()

        body = request.content.read()

        # Check if the post body is valid
        # We have to send both body and request because request.content.read() dumps the body content and we lose it
        # afterwards
        if not self.validate_body(request, body):
            response = {
                'status': 400,
                'message': 'The body of the POST request is malformed. Please see the documentation.'
            }

            request.write(json.dumps(response))
            request.finish()

            return server.NOT_DONE_YET

        # Create the entry body
        entry = Entry.from_json(body)

        d = data_manager.create_doc(entry, cb=cb, eb=eb)

        return server.NOT_DONE_YET


class DeleteEntry(Resource):
    """
    Resource representing the delete_doc() method.
    API endpoint: /delete
    """
    isLeaf = True


    def render_DELETE(self, request):

        def url_is_valid(url):
            """
            Makes sure the "postpath" url only contains one URL segment depicting the <entry_id> parameter
            :param url: url after the /entry/ segment
            """

            if len(url) is not 1:
                return False
            else:
                return True

        def cb(data):
            request.setResponseCode(200)  # Request was successful

            response = {
                'status': 200,
                'message': 'Entry was deleted successfully.'
            }

            request.write(json.dumps(response))
            request.finish()

        def eb(failure):
            request.setResponseCode(404)  # Conflict in the current state of the resource, e.g. might not exist

            response = {
                'status': 404,
                'message': 'Could not find an entry by that name.'
            }

            request.write(json.dumps(response))
            request.finish()

        # Get postpath url segments
        url = request.postpath

        # Check that the URL only contains one additional segment
        if url_is_valid(url):

            # Set response headers for the application/json content-type
            request.responseHeaders.addRawHeader(b'content-type', b'application/json')

            # Get the url parameter
            entry_id = url.pop()

            # Delete the entry
            data_manager.delete_doc(entry_id, cb=cb, eb=eb)

        else:
            request.setResponseCode(400)  # Bad request, URL is not properly formatted

            response = {
                'status': 400,
                'message': 'An invalid URL was provided. Please see documentation.' # TODO: documentation.
            }

            request.write(json.dumps(response))
            request.finish()

        return server.NOT_DONE_YET


root = Resource()

# Resource tree
root.putChild("list", ListEntries())
root.putChild("entry", GetEntry())
root.putChild("add", AddEntry())
root.putChild("delete", DeleteEntry())

application = Application("Syn HTTP Daemon")

from twisted.internet.error import CannotListenError

try:
    reactor.listenTCP(port, Site(root))

# If port is busy
except CannotListenError, e:

    # TODO: check this out, it's leaving the printer open for some reason.
    # Maybe it has to do because the output is coming from this child process?
    print('[Error] Port {0} is in use. Try a different one.'.format(port))
    print('Database has been closed')

    close_db()
    sys.exit(1)
