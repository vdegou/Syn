import logging
import pytest
import registration
import os
import json

from tests import utils
from data import DataManager, Entry
from syn_utils import create_client_from_file


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


uuid = "test_db"
passphrase = u'passphrase'
db_path = os.path.expanduser('~/.syn/dbs/' + uuid)


@pytest.fixture(scope='module')
def setup_environment():
    """
    Function used once before all tests to set up test environment as well
    as finalizer function for cleanup after running all tests.
    """
    # Create the test database
    logger.info('[setup] Creating test database')
    # registration.new_db(uuid, passphrase)

    # Close any open databases
    logger.info('[setup] Closing any open database instances')
    if registration.db_is_open():
        registration.close_db()

    # Open test database
    logger.info('[setup] Opening test database')
    registration.open_db(uuid, passphrase)

    # Create Soledad client object from open test database
    client = create_client_from_file()

    # Return the DataManager object
    yield DataManager(client)

    # logger.info('[teardown] Closing any open database instances')
    # if registration.db_is_open():
    #     registration.close_db()
    #
    # logger.info('[teardown] Removing traces of test databases')
    # if os.path.exists(db_path):
    #     utils.remove_path(db_path)


# @pytest.fixture(scope='module')
# def setup_environment_temp():
#     """
#     Temporary setup function to open premade test database
#     """
#     # Close any open databases
#     logger.info('[setup] Closing any open database instances')
#     if registration.db_is_open():
#         registration.close_db()
#
#     # Open test database
#     logger.info('[setup] Opening test database')
#     registration.open_db('test_dm_db', u'passphrase')
#
#     # Create Soledad client object from open test database
#     client = create_client_from_file()
#
#     # Return the DataManager object
#     yield DataManager(client)
#
#     logger.info('[teardown] Closing any open database instances')
#     if registration.db_is_open():
#         registration.close_db()
#
#     logger.info('[teardown] Removing traces of test databases')
#     if os.path.exists(db_path):
#         utils.remove_path(db_path)


# @pytest.fixture(scope='module')
# def insert_entries():
#     """
#     Fixture used to insert entries into the open database, and later remove them by cleaning them up
#     """
#     logger.info('[setup] Inserting entries into the database')
#
#     entry_1 = Entry('entry_1', 'content_1')
#     entry_2 = Entry('entry_2', 'content_2')
#     entry_3 = Entry('entry_3', 'content_3')
#
#     # TODO: relocate this or fix visibility
#     from syn import _create_client_from_file
#
#     client = _create_client_from_file()
#     # yield client.create_doc_from_json(str(entry_1), doc_id=entry_1.name)
#     # client.create_doc_from_json(str(entry_2), doc_id=entry_2.name)
#
#     # yield
#     # logger.info('[teardown] Removing entries from the database')


# def test_list_entries(setup_environment_temp):
#     """
#     Tests that the list_entries() method is returning the correct amount of documents
#     as well as the information regarding them
#     """
#     def cb_test(data):
#         num_docs = data[0]
#         list_docs = data[1]
#
#         # Assert that there are 3 documents in the list
#         assert num_docs == 3
#
#         # Manually assert name and content of each entry
#         assert json.loads(list_docs[0].get_json())['name'] == 'Gmail'
#         assert json.loads(list_docs[0].get_json())['content'] == '12345678'
#         assert json.loads(list_docs[1].get_json())['name'] == 'Google'
#         assert json.loads(list_docs[1].get_json())['content'] == 'QqxmrtUc'
#         assert json.loads(list_docs[2].get_json())['name'] == 'Youtube'
#         assert json.loads(list_docs[2].get_json())['content'] == 'x2fpbL2I'
#
#     data_manager = setup_environment_temp
#
#     data_manager.list_entries(cb_test)


def test_list_entries(setup_environment):
    """
    Tests that the list_entries() method is returning the correct amount of documents
    as well as the information regarding them
    """

    def cb_test(data):
        num_docs = data[0]
        list_docs = data[1]

        assert num_docs == 3

        assert list_docs[0]['name'] == entry_1.name
        assert list_docs[0]['content'] == entry_1.content
        assert list_docs[1]['name'] == entry_2.name
        assert list_docs[1]['content'] == entry_2.content
        assert list_docs[2]['name'] == entry_3.name
        assert list_docs[2]['content'] == entry_3.content


    data_manager = setup_environment

    # First, we add some entries to test against
    # TODO: this crashes the reactor.
    entry_1 = Entry('entry_1', 'content_1')
    entry_2 = Entry('entry_2', 'content_2')
    entry_3 = Entry('entry_3', 'content_3')

    data_manager.create_doc(entry_1)
    data_manager.create_doc(entry_2)
    data_manager.create_doc(entry_3)

    # Now we get the list of entries and add the testing callback
    testdata = data_manager.list_entries()

    print("TD")
    print(testdata)
    print(type(testdata))

    pytest.fail()
