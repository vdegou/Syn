import logging
import pytest
import registration
import os
import json

from tests import utils
from data import DataManager
from syn_utils import create_client_from_file



logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


uuid = "test_db"
passphrase = u'passphrase'
db_path = os.path.expanduser('~/.syn/dbs/' + uuid)


@pytest.fixture(scope='module')
def setup_environment_temp():
    """
    Temporary setup function to open premade test database
    """
    # Close any open databases
    logger.info('[setup] Closing any open database instances')
    if registration.db_is_open():
        registration.close_db()

    # Open test database
    logger.info('[setup] Opening test database')
    registration.open_db('test_dm_db', u'passphrase')

    # Create Soledad client object from open test database
    client = create_client_from_file()

    # Return the DataManager object
    yield DataManager(client)

    logger.info('[teardown] Closing any open database instances')
    if registration.db_is_open():
        registration.close_db()

    logger.info('[teardown] Removing traces of test databases')
    if os.path.exists(db_path):
        utils.remove_path(db_path)


def test_get_entry(setup_environment_temp):

    def cb_test(entry):
        entry_json = json.loads(entry.get_json())

        # Check that it was able to retrieve the Google entry
        assert entry_json['name'] == 'Google'

        # Check that it was able to retrieve the content of the entry
        assert entry_json['content'] == 'QqxmrtUc'

    data_manager = setup_environment_temp

    data_manager.get_entry('Google', cb_test)
