import logging
import pytest
import registration
import os
import json

from tests import utils
from data import DataManager
from data import Entry
from syn_utils import create_client_from_file


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


uuid = "test_db"
passphrase = u'passphrase'
db_path = os.path.expanduser('~/.syn/dbs/' + uuid)


@pytest.fixture(scope='module')
def setup_environment():
    """
    Function used once before all tests to set up test environment as well
    as finalizer function for cleanup after running all tests.
    """

    # TODO: relocate this function elsewhere


    # Create the test database
    logger.info('[setup] Creating test database')
    registration.new_db(uuid, passphrase)

    # Close any open databases
    logger.info('[setup] Closing any open database instances')
    if registration.db_is_open():
        registration.close_db()

    # Open test database
    logger.info('[setup] Opening test database')
    registration.open_db(uuid, passphrase)

    # Create Soledad client object from open test database
    client = create_client_from_file()

    # Return the DataManager object
    yield DataManager(client)

    logger.info('[teardown] Closing any open database instances')
    if registration.db_is_open():
        registration.close_db()

    logger.info('[teardown] Removing traces of test databases')
    if os.path.exists(db_path):
        utils.remove_path(db_path)


def test_create_doc(setup_environment):
    """
    Test that the create_doc() function actually creates an entry and stores it in the database
    """

    def cb_test(data):
        document = data

        entry_data = json.loads(document.get_json())

        assert entry_data['name'] == entry_name
        assert entry_data['content'] == entry_content
        assert entry_data['timestamp_creation'] == entry_creation
        assert entry_data['last_modified'] == entry_modification
        assert entry_data['uuid'] == entry_uuid

    # Obtain the DataManager from the fixture
    data_manager = setup_environment

    # Create the Entry to store
    entry_name = 'test_name'
    entry_content = 'test_content'
    entry_creation = 100
    entry_modification = 120
    entry_uuid = 'test_uuid'

    entry = Entry(entry_name, entry_content, entry_modification, entry_creation, entry_uuid)

    # Store the entry
    data_manager.create_doc(entry, cb_test)
