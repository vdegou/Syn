import logging
import pytest
import time
import json
import generator

from data import Entry

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def test_from_json():
    """
    Test that passing a properly formatted json to the constructor creates a valid Entry object
    """
    # Create dict structure holding Entry values
    entry_dict = {
        "name": "test_entry",
        "content": "passphrase",
        "timestamp_creation": int(time.time()),
        "last_modified": int(time.time()),
        "uuid": "entryID"
    }

    # Convert dict to json formatted string
    json_string = json.dumps(entry_dict)

    entry = Entry.from_json(json_string)

    assert isinstance(entry, Entry)

    assert entry.name == entry_dict["name"]
    assert entry.content == entry_dict["content"]
    assert entry.timestamp_creation == entry_dict["timestamp_creation"]
    assert entry.last_modified == entry_dict["last_modified"]
    assert entry.uuid == entry_dict["uuid"]


def test_constructor():
    """
    Test that the constructor properly follows all possible instantiation paths correctly
    """

    # Testing with default arguments
    name = 'test_entry'
    content = 'test_content'

    time_before = int(time.time())
    entry = Entry(name, content)
    time_after = int(time.time())

    assert entry.name == name
    assert entry.content == content

    assert time_before <= entry.timestamp_creation <= time_after
    assert time_before <= entry.last_modified <= time_after

    # TODO: should probably find a way to assert that the function generator.generate_id() was called
    assert entry.uuid is not None

    # Testing with all parameters specified
    uuid = 'test_uuid'

    entry = Entry(name,
                  content,
                  timestamp_creation=time_before,
                  last_modified=time_after,
                  uuid=uuid)

    assert entry.name == name
    assert entry.content == content
    assert entry.timestamp_creation == time_before
    assert entry.last_modified == time_after
    assert entry.uuid == uuid