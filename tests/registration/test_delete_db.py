import os
import logging
import pytest

import registration
from tests import utils

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


uuid = "test_db"
passphrase = u'passphrase'
db_path = os.path.expanduser('~/.syn/dbs/' + uuid)


@pytest.fixture(scope='function')
def setup_environment(request):
    """
    Function used once before all tests to set up test environment as well
    as finalizer function for cleanup after running all tests.
    """
    logger.info('[setup] Creating test database')
    registration.new_db(uuid, passphrase)

    logger.info('[setup] Closing any open database instances')
    if registration.db_is_open():
        registration.close_db()

    yield
    # Finalizer function starts after the yield

    logger.info('[teardown] Closing any open database instances')
    if registration.db_is_open():
        registration.close_db()

    logger.info('[teardown] Removing traces of test databases')
    if os.path.exists(db_path):
        utils.remove_path(db_path)


def test_delete_db(setup_environment):
    """
    Check that the delete_db() method in fact deletes the database files.
    """

    # Check that the db was created
    assert os.path.exists(db_path)

    # Try to delete the db
    registration.delete_db(uuid, passphrase)

    # Check that the db path is deleted
    assert not os.path.exists(db_path)


def test_delete_db_throws_exception_when_wrong_credentials(setup_environment):
    """
    Make sure delete_db() throws an InvalidCredentialsException or EnvironmentError
    if either the uuid or passphrase do not match any database
    """

    # Check that the db was created
    assert os.path.exists(db_path)

    # If UUID is of a non-existent database, secrets.json will not be found
    with pytest.raises(EnvironmentError):
        registration.delete_db('wrong_uuid', passphrase)

    # If UUID is of an existent database but passphrase doesn't match
    with pytest.raises(registration.InvalidCredentialsException):
        registration.delete_db(uuid, u'wrong_passphrase')

        # Check that the database was not deleted
        assert os.path.exists(db_path)


def test_delete_db_throws_exception_no_secrets(setup_environment):
    """
    Make sure delete_db() throws an EnvironmentError if there is no secrets.json within the db directory
    """

    # Remove secrets.json from the db directory
    utils.remove_path(db_path + '/secrets.json')

    # Check that the secrets.json file was removed successfully
    assert not os.path.exists(db_path + '/secrets.json')

    with pytest.raises(EnvironmentError):
        registration.delete_db(uuid, passphrase)