import os
import logging
import pytest

import registration
from tests import utils

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


uuid = "test_db"
passphrase = u'passphrase'
db_path = os.path.expanduser('~/.syn/dbs/' + uuid)


@pytest.fixture(scope='module')
def setup_environment(request):
    """
    Function used once before all tests to set up test environment as well
    as finalizer function for cleanup after running all tests.
    """
    logger.info('[setup] Creating test database')
    registration.new_db(uuid, passphrase)

    logger.info('[setup] Closing any open database instances')
    if registration.db_is_open():
        registration.close_db()

    yield
    # Finalizer function starts after the yield

    logger.info('[teardown] Closing any open database instances')
    if registration.db_is_open():
        registration.close_db()

    logger.info('[teardown] Removing traces of test databases')
    if os.path.exists(db_path):
        utils.remove_path(db_path)


def test_close_db_deletes_auth_file(setup_environment):
    """
    Test that close_db() removes the auth_file created when a database is opened
    """

    # Open the database
    if not registration.db_is_open():
        registration.open_db(uuid, passphrase)

    # Check that a database is open by looking for an auth_file
    auth_file_path = os.path.expanduser('~/.syn/auth_file')
    assert os.path.exists(auth_file_path)

    # Close the database
    registration.close_db()

    # Check that the auth_file was removed when the database closes
    assert not os.path.exists(auth_file_path)


def test_close_db_when_already_closed(setup_environment):
    """
    Test that close_db() raises a DatabaseException when attempting to close without
    having an open database
    """

    # Check that there is no database open
    assert not registration.db_is_open()

    with pytest.raises(registration.DatabaseException):
        # Attempt to close database
        registration.close_db()
