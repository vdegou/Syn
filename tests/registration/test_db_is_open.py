import os
import logging
import pytest

import registration
from tests import utils

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


uuid = "test_db"
passphrase = u'passphrase'
db_path = os.path.expanduser('~/.syn/dbs/' + uuid)


@pytest.fixture(scope='module')
def setup_environment(request):
    """
    Function used once before all tests to set up test environment as well
    as finalizer function for cleanup after running all tests.
    """
    logger.info('[setup] Creating test database')
    registration.new_db(uuid, passphrase)

    logger.info('[setup] Closing any open database instances')
    if registration.db_is_open():
        registration.close_db()

    yield
    # Finalizer function starts after the yield

    logger.info('[teardown] Closing any open database instances')
    if registration.db_is_open():
        registration.close_db()

    logger.info('[teardown] Removing traces of test databases')
    if os.path.exists(db_path):
        utils.remove_path(db_path)


def test_db_is_open(setup_environment):
    """
    Test that the function returns True when a database is open, and false when it is not
    """
    # First, we check that there are no open databases
    # Check manually first with native python libs
    auth_file_path = os.path.expanduser('~/.syn/auth_file')
    assert not os.path.exists(auth_file_path)

    # Check with the tested function
    assert not registration.db_is_open()

    # Open a database
    registration.open_db(uuid, passphrase)

    # Check manually
    assert os.path.exists(auth_file_path)

    # Check with the tested function
    assert registration.db_is_open()
