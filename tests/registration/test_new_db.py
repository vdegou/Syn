import os
import logging
import pytest

import registration
from tests import utils

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


uuid = "test_db"
passphrase = u'passphrase'
db_path = os.path.expanduser('~/.syn/dbs/' + uuid)


@pytest.fixture(scope='module')
def setup_environment(request):
    """
    Function used once before all tests to set up test environment as well
    as finalizer function for cleanup after running all tests.
    """
    yield
    # Finalizer function starts after the yield
    logger.info('[teardown] Closing any open database instances')
    if registration.db_is_open():
        registration.close_db()

    logger.info('[teardown] Removing traces of test databases')
    if os.path.exists(db_path):
        utils.remove_path(db_path)


def teardown_function(function):

    # If a test database was created in a test case, remove it
    if os.path.exists(db_path):
        utils.remove_path(db_path)


def test_new_db_creates_db_files(setup_environment):
    """
    Test that the database files are created when calling function
    """
    registration.new_db(uuid, passphrase)

    db_file = db_path + '/db.sqlite'
    secrets_file = db_path + '/secrets.json'

    assert os.path.exists(db_path)
    assert os.path.exists(db_file)
    assert os.path.exists(secrets_file)


def test_new_db_same_uuid_throws_exception(setup_environment):
    """
    Tests that attempting to create a database with the uuid of an already
    existing database throws an exception
    """
    with pytest.raises(registration.DatabaseException):
        # Create first db
        registration.new_db(uuid, passphrase)

        # Create second db using same uuid and passphrase
        registration.new_db(uuid, passphrase)

    # Test that the same exception is raised even with a different passphrase
    with pytest.raises(registration.DatabaseException):

        #Create first db
        registration.new_db(uuid, u'different_passphrase')
