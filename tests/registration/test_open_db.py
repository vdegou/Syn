import os
import json
import registration

import pytest
import logging
from tests import utils

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


uuid = "test_db"
passphrase = u'passphrase'
db_path = os.path.expanduser('~/.syn/dbs/' + uuid)


@pytest.fixture(scope='module')
def setup_environment(request):
    """
    Function used once before all tests to set up test environment as well
    as finalizer function for cleanup after running all tests.
    """
    logger.info('[setup] Creating test database')
    registration.new_db(uuid, passphrase)

    logger.info('[setup] Closing any open database instances')
    if registration.db_is_open():
        registration.close_db()

    yield
    # Finalizer function starts after the yield

    logger.info('[teardown] Closing any open database instances')
    if registration.db_is_open():
        registration.close_db()

    logger.info('[teardown] Removing traces of test databases')
    if os.path.exists(db_path):
        utils.remove_path(db_path)


def setup_function(function):
    if registration.db_is_open():
        registration.close_db()


def teardown_function(function):
    if registration.db_is_open():
        registration.close_db()


def test_open_db_create_auth_file(setup_environment):
    """
    Check that when a database is opened, the corresponding auth file is created
    """
    logger.info('\n[test] open_db_create_auth_file()')
    registration.open_db(uuid, passphrase)

    # Check that the auth_file was created
    auth_file_path = os.path.expanduser('~/.syn/auth_file')
    assert os.path.exists(auth_file_path)

    # Check that the content of the auth_file matches the db metadata
    with open(auth_file_path, 'r') as auth:
        db_info = auth.read()
        db_config = json.loads(db_info)

        assert uuid == db_config['uuid']
        assert passphrase == db_config['passphrase']


def test_open_db_already_open(setup_environment):
    """
    Check that the proper Exception is thrown when a database is opened while
    another one is already open.
    """
    logger.info('\n[test] open_db_already_open()')
    with pytest.raises(registration.DatabaseException) as e:

        registration.open_db(uuid, passphrase)
        registration.open_db(uuid, passphrase)
