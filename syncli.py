import datetime
import requests

import click

import data
import generator
import registration
from syn_utils import copy_to_clipboard


@click.group()
def cli():
    """
    Syn is a password manager which offers an easy to use interface and cryptographically secure storage of passwords
    and notes.
    """
    pass


@cli.command()
@click.option('--length', default=8, help='Specifies the amount of characters in the passphrase')
@click.option('--seed', default=None, help='Provides a seed to make password generation follow a deterministic process')
def passphrase(length, seed):
    """
    Generates a passphrase
    """
    # If seed is provided, generate password with deterministic approach
    if seed:
        passphrase = generator.generate_password_deterministic(length, seed)

    # If no seed. use non-deterministic approach
    else:
        passphrase = generator.generate_password(length)

    click.echo("Passphrase is: %s" % passphrase)


@cli.command(name='new')
def new_db():
    """
    Creates a new database
    """
    click.echo("Creating a new database...")
    uuid = click.prompt('UUID', type=str)
    passphrase = click.prompt('Passphrase', type=unicode, hide_input=True, confirmation_prompt=True)

    try:
        registration.new_db(uuid, passphrase)

        click.echo('Successfully created a database!')

    except registration.DatabaseException as e:
        error_message = e[0]
        click.echo(error_message)


@cli.command()
def delete_db():
    """
    Deletes a database
    """
    if not registration.db_is_open():
        click.echo('Deleting a database... Press CTRL+C to abort.')

        uuid = click.prompt('UUID', type=str)
        passphrase = click.prompt('Passphrase', type=unicode, hide_input=True, confirmation_prompt=True)

        try:
            registration.delete_db(uuid, passphrase)
            click.echo("Database has been deleted.")

        except (registration.InvalidCredentialsException, EnvironmentError):
            click.echo("Invalid credentials. Could not delete database.")

    else:
        click.echo('Cannot delete a database while one is open.')


@cli.command(name='open')
def open_db():
    """
    Opens the database
    """
    if not registration.db_is_open():
        uuid = click.prompt('UUID', type=str)
        passphrase = click.prompt('Passphrase', type=unicode, hide_input=True)

        try:
            registration.open_db(uuid, passphrase)

            click.echo("Database has been opened")

        except Exception as e:
            click.echo(e)

    else:
        click.echo("A database is already open")


@cli.command(name="close")
def close_db():
    """
    Closes a database
    """
    try:
        registration.close_db()
        click.echo("Database has been closed")

    except Exception as e:
        click.echo(e)


@cli.command(name='add')
def add_entry():
    """
    Store a new entry in the database
    """
    entry_name = click.prompt("Name of the entry you wish to create", type=str)

    if click.confirm("Would you like to randomly generate a passphrase?"):
        length = click.prompt("Enter the desired length of the passphrase", type=int, default=8)
        seed = click.prompt("Enter a generation seed", default="", type=str)

        # If seed was set, use deterministic passphrase generation
        if seed != "":
            passphrase = generator.generate_password_deterministic(length, seed)

        # Else use non-deterministic passphrase generation
        else:
            passphrase = generator.generate_password(length)
    else:
        passphrase = click.prompt("Enter the desired passphrase", hide_input=True, confirmation_prompt=True)

    # Create entry
    entry = data.Entry(entry_name, passphrase)

    entry_json = entry.__str__()

    res = requests.request(method='POST', url='http://localhost:5000/add', data=entry_json)
    result = res.json()

    if res.status_code == 200:
        click.echo(result['message'])

    # Conflict when inserting the entry
    elif res.status_code == 409:
        click.echo("There was a conflict when trying to insert the entry.")


@cli.command(name='delete')
@click.option('--name', type=str)
def delete_entry(name):
    """
    Delete an entry from the database
    """
    if not name:
        name = click.prompt("Name of the entry you wish to delete", type=str)

    res = requests.request(method='DELETE', url='http://localhost:5000/delete/{}'.format(name))
    result = res.json()

    if res.status_code == 200:
        click.echo(result['message'])

    # Resource not found
    elif res.status_code == 404:
        click.echo(result['message'])


@cli.command(name="list")
def list_entries():
    """
    Lists the names of stored entries
    """
    res = requests.get('http://localhost:5000/list')

    # If request went ok
    if res.status_code == 200:
        result = res.json()

        entries = result['entries']

        click.echo('Found {0} entries:'.format(len(entries)))

        for entry in entries:
            click.echo(entry)


@cli.command(name='get')
@click.option('--name', type=str)
def get_entry(name):
    """
    Shows the details of an entry
    """
    if not name:
        name = click.prompt("Name of the entry you wish to obtain", type=str)

    res = requests.get('http://localhost:5000/entry/{}'.format(name))
    result = res.json()

    # If request went ok
    if res.status_code == 200:

        click.echo('Found the following entry:'.format(result['name']))

        # Horizontal line
        click.echo(''.join(u'\u2500' for x in range(25)))

        click.echo("Name: {}".format(result['name']))
        click.echo(
            "Time of creation: {} UTC".format(datetime.datetime.utcfromtimestamp(result['timestamp_creation'])))
        click.echo(
            "Time of last modification: {} UTC".format(datetime.datetime.utcfromtimestamp(result['last_modified'])))

        # TODO: look for a purely pythonic way of copying to clipboard. Right now we depend on xclip
        # Check if the xclip utility is installed
        content = copy_to_clipboard(str(result['content']))

        if content:
            click.echo("Contents of the entry have been copied to the system clipboard!")

        else:
            click.echo("The xclip utility is not installed. Please install it so we can copy the contents of the"
                       "entry to your clipboard.")

        # Horizontal line
        click.echo(''.join(u'\u2500' for x in range(25)))


    # Could not find an entry by that name
    elif res.status_code == 404:
        click.echo(result['message'])
